//
//  Enviroment.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 26.02.2022.
//

import Foundation

enum Enviroment {
    case production

    var baseURL: String {
        switch self {
        case .production:
            return "http://www.alarstudios.com/"
        }
    }
}

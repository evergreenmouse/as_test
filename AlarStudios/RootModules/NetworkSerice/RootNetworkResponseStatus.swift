//
//  RootNetworkError.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 26.02.2022.
//

import Foundation

protocol RootNetworkResponseStatus: Codable {
    var status: String { get }
}

extension RootNetworkResponseStatus {
    var isValid: Bool {
        return status == "ok"
    }
}

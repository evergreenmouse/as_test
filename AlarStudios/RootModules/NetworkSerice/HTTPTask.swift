//
//  HTTPTask.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 26.02.2022.
//

import Foundation

typealias HTTPHeaders = [String: String]
typealias Parameters = [String: String]

enum HTTPTask {
    case request
}

//
//  NetworkService.swift
//  AlarStudios 
//
//  Created by Ополовников Александр Николаевич on 26.02.2022.
//

import Foundation

class NetworkService {
    static let instanse = NetworkService()
    let session: URLSession = URLSession(configuration: URLSessionConfiguration.default)

    var task: URLSessionTask?

    func request(_ request: RootRequest, completion: @escaping (Data?) -> Void) {
        guard let url = request.asURLRequest() else { return }
        task = session.dataTask(with: url) { data, response, error in
            if let _ = error {
                completion(nil)
            }
            if let data = data {
                completion(data)
            }
        }
        task?.resume()
    }
}

//
//  RootRequest.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 26.02.2022.
//

import Foundation

protocol RootRequest {
    var baseUrl: String { get }
    var path: String { get }
    var method: HTTPMethod { get }
    var headers: HTTPHeaders? { get }
    var queryItems: [URLQueryItem]? { get }
    var parameters: Parameters? { get }
}

extension RootRequest {
    func asURLRequest() -> URL? {
        switch method {
        case .get:
            var urlComponents = URLComponents(string: "\(baseUrl)\(path)")
            urlComponents?.queryItems = queryItems
            return urlComponents?.url
        }
    }
}

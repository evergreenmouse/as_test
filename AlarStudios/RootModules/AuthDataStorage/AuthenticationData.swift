//
//  AuthenticationData.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 25.02.2022.
//

import Foundation

final class AuthenticationData {
    private enum StoreKeys: String {
        case code = "ud_code"
    }

    static var userCode: String? {
        get {
            return UserDefaults.standard.string(forKey: StoreKeys.code.rawValue)
        }
        set {
            UserDefaults.standard.set(newValue, forKey: StoreKeys.code.rawValue)
        }
    }

    static func isAuthorized() -> Bool {
        return userCode != nil
    }
}

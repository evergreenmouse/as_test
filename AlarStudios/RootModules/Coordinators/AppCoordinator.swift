//
//  AppCoordinator.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 25.02.2022.
//

import UIKit

final class AppCoordinator: Coordinator {
    var navigationController: UINavigationController?

    private let window: UIWindow

    init(with window: UIWindow) {
        self.window = window
    }

    func start() {
        guard let navigationController = configureScene() as? UINavigationController else { return }
        self.navigationController = navigationController

        let userIsAuthorized = AuthenticationData.isAuthorized()
        let coordinator: Coordinator = userIsAuthorized ? MainSceneCoordinator(with: navigationController) : AuthSceneCoordinator(with: navigationController)
        coordinator.start()
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }

    func configureScene() -> UIViewController {
        return UINavigationController()
    }
}

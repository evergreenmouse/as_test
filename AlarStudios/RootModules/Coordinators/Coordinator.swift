//
//  Coordinator.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 25.02.2022.
//

import UIKit

protocol Coordinator: AnyObject {
    var navigationController: UINavigationController? { get }
    func start()
    func configureScene() -> UIViewController
}



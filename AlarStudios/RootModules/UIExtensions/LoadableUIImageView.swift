//
//  LoadableUIImageView.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 26.02.2022.
//

import UIKit

extension UIImageView {
    func loadRandomImage(with guid: String) {
        self.image = nil
        let nsGuid = NSString(string: guid)
        if let imageFromCache = ImageCache.cache.object(forKey: nsGuid) {
            self.image = imageFromCache
            return
        }
            
        ImageLoaderNetworkService.instance.fetchRandomImage(guid: guid, completion: { imageData in
            guard let imageData = imageData else {
                return
            }
            let imageToCache = UIImage(data: imageData) ?? UIImage()
            ImageCache.cache.setObject(imageToCache, forKey: nsGuid)
            DispatchQueue.main.async {
                self.image = imageToCache
            }
        })
    }

    func dropLoadTask(with guid: String) {
        ImageLoaderNetworkService.instance.session.getAllTasks { tasks in
            if let task = tasks.first(where: { $0.originalRequest?.url?.absoluteString.contains(guid) ?? true }) {
                task.cancel()
            }
        }
    }
}

//
//  ImageLoaderAPIRouter.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 26.02.2022.
//

import Foundation

enum ImageLoaderAPIRouter: RootRequest {
    case fetchRandomImage(guid: String)

    var baseUrl: String {
        return "https://picsum.photos/"
    }
    
    var path: String {
        return "100/100"
    }
    
    var method: HTTPMethod {
        return .get
    }
    
    var headers: HTTPHeaders? {
        switch self {
        case .fetchRandomImage:
            return nil
        }
    }
    
    var queryItems: [URLQueryItem]? {
        switch self {
        case .fetchRandomImage(let guid):
            return [URLQueryItem(name: "random", value: guid)]
        }
    }
    
    var parameters: Parameters? {
        return nil
    }
    
    
}

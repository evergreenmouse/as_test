//
//  ImageCache.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 26.02.2022.
//

import UIKit

final class ImageCache {
    static let cache = NSCache<NSString, UIImage>()
}

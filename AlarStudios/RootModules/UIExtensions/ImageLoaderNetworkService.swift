//
//  ImageLoaderNetworkService.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 26.02.2022.
//

import Foundation

final class ImageLoaderNetworkService: NetworkService {
    static let instance = ImageLoaderNetworkService()

    func fetchRandomImage(guid: String, completion: @escaping (Data?) -> Void) {
        request(ImageLoaderAPIRouter.fetchRandomImage(guid: guid)) { data in
            guard let data = data else {
                completion(nil)
                return
            }
            completion(data)
        }
    }
}

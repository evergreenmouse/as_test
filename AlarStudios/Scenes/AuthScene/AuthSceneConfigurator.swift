//
//  AuthSceneConfigurator.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 26.02.2022.
//

import UIKit

final class AuthSceneConfigurator {

    init() { }

    func configure(with viewController: AuthSceneViewController, coordinator: AuthSceneCoordinator) {
        let viewModel = AuthSceneViewModel()
        viewController.viewModel = viewModel
        viewModel.viewController = viewController
        viewModel.coordinator = coordinator
    }
}

//
//  AuthSceneNetworkModel.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 26.02.2022.
//

import Foundation

struct AuthSceneMetworkModel: Codable, RootNetworkResponseStatus {
    var status: String
    let code: String
}

//
//  AuthSceneNetworkService.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 26.02.2022.
//

import Foundation

final class AuthSceneNetworkService: NetworkService {
    func authUser(username: String, password: String, completion: @escaping (AuthSceneMetworkModel?) -> Void) {
        request(AuthSceneAPIRouter.loginUser(username: username, password: password), completion: { response in
            guard let response = response else { return }
            do {
                let model = try JSONDecoder().decode(AuthSceneMetworkModel.self, from: response)
                completion(model.isValid ? model : nil)
            } catch {
                completion(nil)
            }
        })
    }
}

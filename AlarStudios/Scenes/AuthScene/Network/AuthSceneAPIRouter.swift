//
//  AuthSceneAPIRouter.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 26.02.2022.
//

import Foundation

enum AuthSceneAPIRouter: RootRequest {
    
    case loginUser(username: String, password: String)

    var baseUrl: String {
        return Enviroment.production.baseURL
    }
    
    var path: String {
        switch self {
        case .loginUser:
            return "test/auth.cgi"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .loginUser:
            return .get
        }
    }
    
    var headers: HTTPHeaders? {
        switch self {
        case .loginUser:
            return nil
        }
    }

    var queryItems: [URLQueryItem]? {
        switch self {
        case .loginUser(let username, let password):
            return [URLQueryItem(name: "username", value: username),
                    URLQueryItem(name: "password", value: password)]
        }
    }
    
    var parameters: Parameters? {
        switch self {
        case .loginUser:
            return nil
        }
    }
}

//
//  AuthSceneCoordinator.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 25.02.2022.
//

import UIKit

final class AuthSceneCoordinator: Coordinator {
    var navigationController: UINavigationController?

    init(with navigationController: UINavigationController?) {
        self.navigationController = navigationController
    }
    
    func start() {
        let viewController = configureScene()
        navigationController?.pushViewController(viewController, animated: true)
    }

    func goToMain() {
        let mainSceneCoordinator = MainSceneCoordinator(with: navigationController)
        mainSceneCoordinator.start()
        navigationController?.viewControllers.removeFirst()
    }

    func configureScene() -> UIViewController {
        let viewController = AuthSceneViewController()
        AuthSceneConfigurator().configure(with: viewController, coordinator: self)
        return viewController
    }
}

//
//  AuthViewController.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 25.02.2022.
//

import UIKit

final class AuthSceneViewController: UIViewController {
    var viewModel: AuthSceneViewOutput?

    //MARK: - UI Elements
    private lazy var logoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(named: "startingCat") ?? UIImage()
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private lazy var usernameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.text = "Username"
        return label
    }()

    private lazy var usernameTextField: UITextField = {
        let textField = UITextField()
        textField.borderStyle = .roundedRect
        textField.placeholder = "John Doe"
        return textField
    }()

    private lazy var passwordLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .left
        label.text = "Password"
        return label
    }()

    private lazy var passwordTextField: UITextField = {
        let textField = UITextField()
        textField.borderStyle = .roundedRect
        textField.clearButtonMode = .whileEditing
        textField.placeholder = "∘∘∘∘∘"
        textField.isSecureTextEntry = true
        textField.keyboardType = .numberPad
        
        return textField
    }()

    private lazy var loginButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = .blue
        button.setTitleColor(.white, for: .normal)
        button.setTitle("GO", for: .normal)
        button.layer.cornerRadius = 8
        button.addTarget(self, action: #selector(loginButtonWasPressed(_:)), for: .touchUpInside)
        return button
    }()

    private lazy var errorLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.text = "Incorrect login/password"
        label.textColor = .red
        label.font = .systemFont(ofSize: 13)
        label.isHidden = true
        return label
    }()

    private lazy var activityIndicator: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.hidesWhenStopped = true
        view.style = .medium
        return view
    }()

    //MARK: StackViews
    private lazy var authStack: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        return stackView
    }()

    private lazy var usernamePasswordStack: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 6
        return stackView
    }()

    private lazy var passwordStack: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = 8
        return stackView
    }()

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }

    // MARK: - Private methods
    private func setupViews() {
        view.backgroundColor = .white
        view.addSubview(logoImageView)
        view.addSubview(authStack)
        view.addSubview(errorLabel)
        view.addSubview(activityIndicator)

        NSLayoutConstraint.activate([logoImageView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
                                     logoImageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 16),
                                     logoImageView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
                                     logoImageView.bottomAnchor.constraint(equalTo: authStack.topAnchor, constant: -16),

                                     loginButton.widthAnchor.constraint(equalToConstant: 64),

                                     authStack.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
                                     authStack.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
                                     authStack.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -32),

                                     errorLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
                                     errorLabel.topAnchor.constraint(equalTo: authStack.bottomAnchor, constant: 12),
                                     errorLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),

                                     activityIndicator.centerXAnchor.constraint(equalTo: errorLabel.centerXAnchor),
                                     activityIndicator.centerYAnchor.constraint(equalTo: errorLabel.centerYAnchor)])

        usernamePasswordStack.addArrangedSubview(usernameLabel)
        usernamePasswordStack.addArrangedSubview(usernameTextField)
        usernamePasswordStack.addArrangedSubview(passwordLabel)
        usernamePasswordStack.addArrangedSubview(passwordStack)
        
        passwordStack.addArrangedSubview(passwordTextField)
        passwordStack.addArrangedSubview(loginButton)
        
        authStack.addArrangedSubview(usernamePasswordStack)
    }

    @objc private func loginButtonWasPressed(_ sender: UIButton) {
        view.endEditing(true)
        activityIndicator.startAnimating()
        loginButton.isEnabled = false
        self.errorLabel.isHidden = true
        viewModel?.loginButtonWasPressed(with: usernameTextField.text?.lowercased() ?? "", password: passwordTextField.text?.lowercased() ?? "")
    }
}

extension AuthSceneViewController: AuthSceneViewModelOutput {
    func viewModel(_ viewModel: AuthSceneViewOutput, didFetchAuthResultWithError error: Error?) {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
            self.loginButton.isEnabled = true
            self.errorLabel.isHidden = false
            self.passwordTextField.text = ""
            self.passwordTextField.becomeFirstResponder()
        }
    }
}

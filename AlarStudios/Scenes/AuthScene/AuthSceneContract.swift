//
//  AuthSceneContract.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 26.02.2022.
//

import Foundation

protocol AuthSceneViewModelOutput: AnyObject {
    func viewModel(_ viewModel: AuthSceneViewOutput, didFetchAuthResultWithError error: Error?)
}

protocol AuthSceneViewOutput: AnyObject {
    func loginButtonWasPressed(with username: String, password: String)
}

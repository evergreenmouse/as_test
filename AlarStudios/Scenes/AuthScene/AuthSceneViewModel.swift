//
//  AuthSceneViewModel.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 25.02.2022.
//

import Foundation

final class AuthSceneViewModel {
    weak var viewController: AuthSceneViewController?
    let networkService = AuthSceneNetworkService()
    var coordinator: AuthSceneCoordinator?
}

extension AuthSceneViewModel: AuthSceneViewOutput {
    func loginButtonWasPressed(with username: String, password: String) {
        networkService.authUser(username: username, password: password) { [weak self] model in
            guard let self = self else { return }
            guard let model = model else {
                self.viewController?.viewModel(self, didFetchAuthResultWithError: nil)
                return
            }
            AuthenticationData.userCode = model.code
            DispatchQueue.main.async {
                self.coordinator?.goToMain()
            }
        }
    }
}

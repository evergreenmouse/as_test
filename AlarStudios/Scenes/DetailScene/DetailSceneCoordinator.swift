//
//  DetailSceneCoordinator.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 26.02.2022.
//

import UIKit

final class DetailSceneCoordinator: Coordinator {
    var navigationController: UINavigationController?
    var model: LocationModel?

    init(with navigationController: UINavigationController?) {
        self.navigationController = navigationController
    }

    func start() {
        let viewController = configureScene()
        navigationController?.pushViewController(viewController, animated: true)
    }

    func configureScene() -> UIViewController {
        guard let model = model else { return UIViewController() }
        let viewController = DetailSceneViewController()
        DetailSceneConfigurator().configure(with: viewController, model: model)
        return viewController
    }
}

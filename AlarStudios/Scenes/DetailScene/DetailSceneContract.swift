//
//  DetailSceneContract.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 26.02.2022.
//

import Foundation
import MapKit

protocol DetailSceneViewModelOutput: AnyObject {
    func reloadTableView()
    func configureMap(with annotation: MKPointAnnotation)
}

protocol DetailSceneViewOutput: AnyObject {
    var tableManager: DetailSceneTableManager { get }

    func viewDidLoad(_ view: DetailSceneViewModelOutput)
}

protocol DetailSceneTableManagerDelegate: AnyObject { }

//
//  DetailSceneViewController.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 26.02.2022.
//

import UIKit
import MapKit

final class DetailSceneViewController: UIViewController {
    var viewModel: DetailSceneViewOutput?
    var model: LocationModel?

    //MARK: - UI Elements
    private lazy var map: MKMapView = {
        let view = MKMapView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        return tableView
    }()

    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        configureTableView()
        registerTableViewCells()
        viewModel?.viewDidLoad(self)
    }

    //MARK: - private methods
    private func setupViews() {
        view.backgroundColor = .white
        view.addSubview(map)
        view.addSubview(tableView)
        NSLayoutConstraint.activate([map.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                                     map.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
                                     map.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                                     map.heightAnchor.constraint(equalToConstant: view.frame.size.height / 2),

                                     tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                                     tableView.topAnchor.constraint(equalTo: map.bottomAnchor, constant: 16),
                                     tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                                     tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)])
    }

    private func configureTableView() {
        tableView.delegate = viewModel?.tableManager
        tableView.dataSource = viewModel?.tableManager
    }

    private func registerTableViewCells() {
        tableView.register(DetailSceneTableViewCell.self, forCellReuseIdentifier: DetailSceneTableViewCell.reuseIdentifier)
    }
}

extension DetailSceneViewController: DetailSceneViewModelOutput {
    func configureMap(with annotation: MKPointAnnotation) {
        map.addAnnotation(annotation)
        let span = MKCoordinateSpan(latitudeDelta: 0.12, longitudeDelta: 0.12)
        let region = MKCoordinateRegion(center: annotation.coordinate, span: span)
        map.setRegion(region, animated: true)
    }
    
    func reloadTableView() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
}

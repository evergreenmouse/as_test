//
//  DetailSceneConfigurator.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 26.02.2022.
//

import Foundation

final class DetailSceneConfigurator {
    func configure(with viewController: DetailSceneViewController, model: LocationModel) {
        let viewModel = DetailSceneViewModel()
        viewController.viewModel = viewModel
        viewModel.viewController = viewController
        viewModel.model = model
    }
}

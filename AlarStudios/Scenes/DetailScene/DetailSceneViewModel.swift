//
//  DetailSceneViewModel.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 26.02.2022.
//

import Foundation
import MapKit

final class DetailSceneViewModel {
    weak var viewController: DetailSceneViewModelOutput?
    var tableManager = DetailSceneTableManager.init()
    var model: LocationModel?

    private func remapModelForTableManager() {
        guard let model = model  else { return }
        tableManager.items = model.getArrayOfSingleProperties()
        viewController?.reloadTableView()
    }

    private func prepareMapAnnotation() {
        guard let model = model else { return }
        let annotation = MKPointAnnotation()
        annotation.coordinate = CLLocationCoordinate2D(latitude: model.lat, longitude: model.lon)
        annotation.title = model.name
        viewController?.configureMap(with: annotation)
    }
}

extension DetailSceneViewModel: DetailSceneViewOutput {
    func viewDidLoad(_ view: DetailSceneViewModelOutput) {
        remapModelForTableManager()
        prepareMapAnnotation()
    }
}

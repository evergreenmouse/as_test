//
//  DetailSceneTableManager.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 26.02.2022.
//

import Foundation
import UIKit

final class DetailSceneTableManager: NSObject {
    var items = [SinglePropertyLocation]()
}

extension DetailSceneTableManager: UITableViewDelegate { }

extension DetailSceneTableManager: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: DetailSceneTableViewCell.reuseIdentifier) as? DetailSceneTableViewCell else {
            return UITableViewCell()
        }
        let model = items[indexPath.row]
        cell.configure(with: model.title, description: model.description)
        return cell
    }
}

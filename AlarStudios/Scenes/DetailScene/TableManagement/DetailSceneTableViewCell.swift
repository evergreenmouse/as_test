//
//  DetailSceneTableViewCell.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 26.02.2022.
//

import UIKit

final class DetailSceneTableViewCell: UITableViewCell {
    static let reuseIdentifier = "\(String(describing: self))"

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 15, weight: .bold)
        label.textAlignment = .left
        label.numberOfLines = 1
        return label
    }()

    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .systemFont(ofSize: 15)
        label.textAlignment = .right
        label.numberOfLines = 0
        return label
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        contentView.addSubview(titleLabel)
        contentView.addSubview(descriptionLabel)

        NSLayoutConstraint.activate([titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
                                     titleLabel.trailingAnchor.constraint(equalTo: descriptionLabel.leadingAnchor, constant: 8),
                                     titleLabel.centerYAnchor.constraint(equalTo: descriptionLabel.centerYAnchor),

                                     descriptionLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
                                     descriptionLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
                                     descriptionLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8)])
    }

    func configure(with title: String, description: String) {
        titleLabel.text = title
        descriptionLabel.text = description
    }
}

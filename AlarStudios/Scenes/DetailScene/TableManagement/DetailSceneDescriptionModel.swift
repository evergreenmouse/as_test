//
//  DetailSceneDescriptionModel.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 26.02.2022.
//

import Foundation

struct DetailSceneDescriptionModel {
    let title: String
    let description: String
}

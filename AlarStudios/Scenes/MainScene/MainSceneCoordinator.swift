//
//  MainSceneCoordinator.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 25.02.2022.
//

import UIKit

final class MainSceneCoordinator: Coordinator {
    var navigationController: UINavigationController?

    init(with navigationController: UINavigationController?) {
        self.navigationController = navigationController
    }

    func start() {
        let viewController = configureScene()
        navigationController?.setViewControllers([viewController], animated: true)
    }

    func goToDetail(pass model: LocationModel) {
        let detailSceneCoordinator = DetailSceneCoordinator(with: navigationController)
        detailSceneCoordinator.model = model
        detailSceneCoordinator.start()
    }

    func configureScene() -> UIViewController {
        let viewController = MainSceneViewController()
        MainSceneConfigurator().configure(with: viewController, coordinator: self)
        return viewController
    }
}

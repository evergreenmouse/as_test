//
//  MainSceneTableViewCell.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 26.02.2022.
//

import UIKit

final class MainSceneTableViewCell: UITableViewCell {
    static let reuseIdentifier = "\(String(describing: self))"
    var guid = ""

    private lazy var posterView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 1
        label.lineBreakMode = .byTruncatingTail
        return label
    }()

    // MARK: - lifecycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        posterView.image = nil
        posterView.dropLoadTask(with: guid)
    }

    // MARK: - private methods
    private func setupViews() {
        contentView.addSubview(posterView)
        contentView.addSubview(titleLabel)
        NSLayoutConstraint.activate([posterView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),
                                     posterView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
                                     posterView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8),
                                     posterView.widthAnchor.constraint(equalToConstant: 64),

                                     titleLabel.leadingAnchor.constraint(equalTo: posterView.trailingAnchor, constant: 16),
                                     titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8),
                                     titleLabel.centerYAnchor.constraint(equalTo: posterView.centerYAnchor)])
    }

    //MARK: - public methods
    public func configure(with model: LocationModel?) {
        guard let model = model else { return }
        titleLabel.text = model.name
        guid = "\(model.id)_\(model.name.replacingOccurrences(of: " ", with: "_"))"
        posterView.loadRandomImage(with: guid)
    }
}

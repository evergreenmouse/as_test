//
//  MainSceneTableManager.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 26.02.2022.
//

import UIKit

final class MainSceneTableManager: NSObject {
    var items = [LocationModel]()
    weak var delegate: MainSceneTableManagerDelegate?
}

extension MainSceneTableManager: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if items.count - 1 == indexPath.row {
            delegate?.fetchMoreLocations()
        }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: MainSceneTableViewCell.reuseIdentifier) as? MainSceneTableViewCell else { return UITableViewCell() }
        cell.configure(with: items[indexPath.row])
        return cell
    }
}

extension MainSceneTableManager: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.didSelectCell(with: items[indexPath.row])
    }
}

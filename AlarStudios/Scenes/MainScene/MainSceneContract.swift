//
//  MainSceneContract.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 26.02.2022.
//

import Foundation

protocol MainSceneViewModelOutput: AnyObject {
    func reloadTableView(from index: Int)
}

protocol MainSceneViewOutput: AnyObject {
    var tableManager: MainSceneTableManager { get }

    func viewDidLoad(_ view: MainSceneViewModelOutput)
}

protocol MainSceneTableManagerDelegate: AnyObject {
    func didSelectCell(with model: LocationModel)
    func fetchMoreLocations()
}

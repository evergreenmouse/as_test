//
//  MainSceneViewModel.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 25.02.2022.
//

import Foundation

final class MainSceneViewModel {
    weak var viewController: MainSceneViewModelOutput?
    var networkService = MainSceneNetworkService()
    var coordinator: MainSceneCoordinator?

    private var currentPage = 1

    lazy var tableManager: MainSceneTableManager = {
        let tableManager = MainSceneTableManager()
        tableManager.delegate = self
        return tableManager
    }()

    private func fetchLocations() {
        networkService.fetchLocations(with: currentPage) { [weak self] model in
            guard let model = model else { return }
            if model.isValid {
                self?.currentPage += 1
                let numberOfItems = self?.tableManager.items.count ?? 0
                self?.handleFetchedResult(with: model.data)
                self?.viewController?.reloadTableView(from: numberOfItems == 0 ? 0 : numberOfItems-1)
            }
        }
    }

    private func handleFetchedResult(with model: [LocationModel]) {
        tableManager.items.append(contentsOf: model)
    }
}

extension MainSceneViewModel: MainSceneViewOutput {
    func viewDidLoad(_ view: MainSceneViewModelOutput) {
        fetchLocations()
    }
}

extension MainSceneViewModel: MainSceneTableManagerDelegate {
    func didSelectCell(with model: LocationModel) {
        coordinator?.goToDetail(pass: model)
    }

    func fetchMoreLocations() {
         fetchLocations()
    }
}

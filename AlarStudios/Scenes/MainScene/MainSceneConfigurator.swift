//
//  MainScreenConfigurator.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 26.02.2022.
//

import UIKit

final class MainSceneConfigurator {
    func configure(with viewController: MainSceneViewController, coordinator: MainSceneCoordinator) {
        let viewModel = MainSceneViewModel()
        viewController.viewModel = viewModel
        viewModel.viewController = viewController
        viewModel.coordinator = coordinator
    }
}

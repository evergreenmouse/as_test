//
//  MainSceneNetworkModel.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 26.02.2022.
//

import Foundation

struct MainSceneNetworkModel: Codable, RootNetworkResponseStatus {
    var status: String
    let data: [LocationModel]
}

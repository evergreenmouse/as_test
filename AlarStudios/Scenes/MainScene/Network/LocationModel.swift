//
//  LocationModel.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 26.02.2022.
//

import Foundation

struct SinglePropertyLocation {
    let title: String
    let description: String
}

struct LocationModel: Codable {
    let id: String
    let name: String
    let country: String
    let lat: Double
    let lon: Double
}

extension LocationModel {
    func getArrayOfSingleProperties() -> [SinglePropertyLocation] {
        var arrayOfSinleProperties = [SinglePropertyLocation]()
        let mirror = Mirror(reflecting: self)

        mirror.children.forEach {
            if let title = $0.label {
                let singleProperty = SinglePropertyLocation(title: title, description: "\($0.value)")
                arrayOfSinleProperties.append(singleProperty)
            }
        }
        return arrayOfSinleProperties
    }
}

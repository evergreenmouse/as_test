//
//  MainSceneNetworkService.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 26.02.2022.
//

import Foundation

final class MainSceneNetworkService: NetworkService {
    func fetchLocations(with pageNumber: Int, completion: @escaping (MainSceneNetworkModel?) -> Void) {
        request(MainSceneAPIRouter.fetchLocations(page: pageNumber)) { response in
            guard let response = response else { return }
            do {
                let model = try JSONDecoder().decode(MainSceneNetworkModel.self, from: response)
                completion(model.isValid ? model : nil)
            } catch {
                completion(nil)
            }
        }
    }
}

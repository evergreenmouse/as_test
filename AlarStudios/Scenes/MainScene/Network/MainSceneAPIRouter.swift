//
//  MainSceneAPIRouter.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 26.02.2022.
//

import Foundation

enum MainSceneAPIRouter: RootRequest {
    case fetchLocations(page: Int)

    var baseUrl: String {
        switch self {
        case .fetchLocations:
            return Enviroment.production.baseURL
        }
    }

    var path: String {
        switch self {
        case .fetchLocations:
            return "test/data.cgi"
        }
    }

    var method: HTTPMethod {
        switch self {
        case .fetchLocations:
            return .get
        }
    }

    var headers: HTTPHeaders? {
        switch self {
        case .fetchLocations:
            return nil
        }
    }

    var queryItems: [URLQueryItem]? {
        switch self {
        case .fetchLocations(let page):
            return [URLQueryItem(name: "code", value: AuthenticationData.userCode ?? ""),
                    URLQueryItem(name: "p", value: "\(page)")]
        }
    }

    var parameters: Parameters? {
        switch self {
        case .fetchLocations:
            return nil
        }
    }
}

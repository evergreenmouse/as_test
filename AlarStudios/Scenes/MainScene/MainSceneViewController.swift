//
//  MainSceneViewController.swift
//  AlarStudios
//
//  Created by Ополовников Александр Николаевич on 25.02.2022.
//

import UIKit

final class MainSceneViewController: UIViewController {
    var viewModel: MainSceneViewOutput?

    //MARK: - UI Elements
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.separatorStyle = .singleLine
        tableView.rowHeight = 100
        return tableView
    }()

    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        configureTableView()
        registerTableViewCells()
        viewModel?.viewDidLoad(self)
    }

    // MARK: - private methods
    private func setupViews() {
        view.backgroundColor = .green
        view.addSubview(tableView)
        NSLayoutConstraint.activate([tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
                                     tableView.topAnchor.constraint(equalTo: view.topAnchor),
                                     tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
                                     tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
    }

    private func configureTableView() {
        tableView.delegate = viewModel?.tableManager
        tableView.dataSource = viewModel?.tableManager
    }

    private func registerTableViewCells() {
        tableView.register(MainSceneTableViewCell.self, forCellReuseIdentifier: MainSceneTableViewCell.reuseIdentifier)
    }
}

extension MainSceneViewController: MainSceneViewModelOutput {
    func reloadTableView(from index: Int) {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}
